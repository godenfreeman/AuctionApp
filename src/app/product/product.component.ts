import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.less']
})
export class ProductComponent implements OnInit {

  private products:Array<Product>

  constructor() { }

  ngOnInit() {
    this.products =[
      new Product("https://via.placeholder.com/120x80", "第一个商品", 1.99, ["电子图书", "产品研发"], "一滴雨，第一个对商品的描述，及对ng的学习", 2.5),
      new Product("https://via.placeholder.com/120x80", "第er个商品", 26.99, ["电子图书", "产品研发"], "一滴雨，第一个对商品的描述，及对ng的学习", 3.5),
      new Product("https://via.placeholder.com/120x80", "第san个商品", 3.99, ["电子图书", "产品研发"], "一滴雨，第一个对商品的描述，及对ng的学习", 1.5),
      new Product("https://via.placeholder.com/120x80", "第4个商品", 4.99, ["电子图书", "产品研发"], "一滴雨，第一个对商品的描述，及对ng的学习", 2),
      new Product("https://via.placeholder.com/120x80", "第五个商品", 5.99, ["电子图书", "产品研发"], "一滴雨，第一个对商品的描述，及对ng的学习", 4),
      new Product("https://via.placeholder.com/120x80", "第陆个商品", .99, ["电子图书", "产品研发"], "一滴雨，第一个对商品的描述，及对ng的学习", 3)
    ]
  }

}

export class Product {
  constructor(
   public imgSrc:string,
   public title:string,
   public price:number,
   public type:Array<string>,
   public describe:string,
   public ratings:number,
  ){
    
  }
}