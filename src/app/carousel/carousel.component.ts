import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.less']
})
export class CarouselComponent implements OnInit {

  private imgSrc:string

  constructor() { }

  ngOnInit() {
    this.imgSrc = "https://via.placeholder.com/1000x400"
  }

}
